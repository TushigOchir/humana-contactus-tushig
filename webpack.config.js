const webpack = require('webpack');
const path = require('path');
const Visualizer = require('webpack-visualizer-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const extractLess = new ExtractTextPlugin({
    filename: "contact.min.css",
    disable: process.env.NODE_ENV === "development"
});

module.exports = {
  entry: {
    main: './src/main.js',
    'vendor.min': './src/vendor.js'
  },
  devServer: {
    contentBase: __dirname + '/dist'
  },
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: '[name].js',
    library: 'humana-contact-us',
    libraryTarget: 'umd'
  },
  module: {
    rules: [{
        test: /\.js$/,
        include: path.resolve(__dirname, 'src'),
        use: {
          loader: 'babel-loader',
          options: {
            presets: ['es2015']
          }
        }
      }, {
        test: /\.less$/,
        use: extractLess.extract({
          use: [{
            loader: 'css-loader'
          }, {
            loader: 'less-loader'
          }],
          fallback: 'style-loader'
        })
      }
    ],
  },
  plugins: [
    new webpack.ProvidePlugin({
      'window.jQuery': 'jquery',
      'window.$': 'jquery',
      $: 'jquery',
      jQuery: 'jquery'
    }),
    extractLess,
    new webpack.optimize.CommonsChunkPlugin({
      names: ['vendor.min'],
      filename: '[name].js',
      minChunks: Infinity
    }),
    new webpack.optimize.UglifyJsPlugin({
      include: /vendor\.min\.js/,
      minimize: true
    })
    // commonsChunk
  ]
};
