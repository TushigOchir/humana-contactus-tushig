(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory();
	else if(typeof define === 'function' && define.amd)
		define([], factory);
	else if(typeof exports === 'object')
		exports["humana-contact-us"] = factory();
	else
		root["humana-contact-us"] = factory();
})(this, function() {
return webpackJsonphumana_contact_us([0],[
/* 0 */,
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";


/*******************************************************************************
 * constants.js
 *
 * This file contains all the constants
 ******************************************************************************/
/**
 * select input options for 'What would you like to do?'
 * if you change the values in the HTML, be sure to update here as well.
 * NOTE: if the value here doesn't match the corresponding <option> element value of the <value> attribute,
 *			  exception 'invalidSelectOption' is thrown.
 */
module.exports = {
  FORM_ID: 'elqForm',
  OPTION_QUOTE: 'Get a quote',
  OPTION_WELLNESS: 'Learn more about wellness',
  OPTION_DISCUSS_PRODUCTS: 'Discuss our products/services',
  OPTION_CUSTOMER_SUPPORT: 'Get customer support',
  EMAIL_REGEX: /[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,9}$/,
  EMAIL_NOT_ALLOWED_REGEX: /^[,()\[\]\\:";<>]+$/,
  /**
   * states subject to RTA routing
   */
  RTA_STATES: ['AZ', 'CO', 'FL', 'GA', 'IL', 'IN', 'KS', 'KY', 'LA', 'MI', 'MO', 'MS', 'NV', 'OH', 'TN', 'TX', 'UT', 'WI'],
  /**
  * states subject to eHealth routing
  */
  /*E_HEALTH_STATES : [
    'CA',
    'NC',
    'NJ',
    'NY'
  ],*/
  SIMPLY_INSURED_STATES: ['VA', 'WA', 'AL', 'PA', 'SC', 'CA', 'NC', 'NJ', 'NY'],
  /**
   * @ALPHABETIC_DASH_REGEX : used for first name / last name input validation
   * 												matches any text that has only alphabetic
   *												characters and dash(-).
   */
  ALPHABETIC_DASH_REGEX: /^[a-zA-Z-]+$/,

  /**
   * @ALPHABETIC_DASH_SPACE_REGEX: used for last name and agent name
   *                               matches any text that has only alphabetic,
   *                               and/or - and space.
   */
  ALPHABETIC_DASH_SPACE_REGEX: /^[a-zA-Z-\s]+$/,

  /*
   * @DATE_REGEX: used for renewal date field
   *                      matches any text that has only alphanumeric and/or
   *                      '-', '/', and ',' characters.
   */
  DATE_REGEX: /^[a-zA-Z0-9,-/]+$/,

  /**
   * @EMAIL_EXTRA_CHARS_REGEX : used for email additional character validation
   *												 matches text that is constructed from any of the
   *												 characters within the brackers (i.e. [...])
   */
  EMAIL_EXTRA_CHARS_REGEX: /^[0-9A-Za-z.,~!@#$%^&*()]+$/
};

/***/ }),
/* 2 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function($) {

__webpack_require__(3);

var _require = __webpack_require__(1),
    RTA_STATES = _require.RTA_STATES,
    SIMPLY_INSURED_STATES = _require.SIMPLY_INSURED_STATES,
    OPTION_QUOTE = _require.OPTION_QUOTE,
    OPTION_CUSTOMER_SUPPORT = _require.OPTION_CUSTOMER_SUPPORT; /***************************************************************************************************************
                                                                 * main.js
                                                                 * Date: 10/06/2017
                                                                 * This file is the project entry point
                                                                 ***************************************************************************************************************/


var dynamicContentLoader = __webpack_require__(4);
var formatter = __webpack_require__(5);
var validator = __webpack_require__(6);

/**
 * Entry point
 */
$(document).ready(function () {
  // handles the logic for showing/hiding dynamic elements
  dynamicContentLoader.setupDynamicInputHandlers();
  // decide where to send information: ehealth or rta
  initRouting();
  // applies constraints/formatting to input elements
  formatter.setupInputFormatting();
  // setup the logic for form validation
  validator.setupFormValidation();
});

/**
 * Depending on user entry, eloqua will send notification email to either
 * 1. Rta
 * 2. eHealth
 * 3. simplyInsured
 */
function initRouting() {
  var $rta = $('#rta');
  var $eHealth = $('#eHealth');
  var $simplyInsured = $('#SimplyInsured');
  $('.representative-routing').on('change', function () {
    var numEmployees = $('#noOfEmployees1').val();
    var usState = $('#txtState').val();
    var contactReason = $('#txtwhy option:selected').text();
    var isWorkingWithBroker = $('#cboBroker').val() === 'Yes';
    var isContactBrokerTrue = $('#cboRegard').val() === 'Yes';
    if (numEmployees <= 50 && contactReason !== OPTION_CUSTOMER_SUPPORT && RTA_STATES.indexOf(usState) !== -1 && (!isWorkingWithBroker || isWorkingWithBroker && !isContactBrokerTrue)) {
      // contact RTA
      $rta.val('yes');
      $eHealth.val('no');
      $simplyInsured.val('no');
    }
    /*else if (numEmployees<=50 &&
              contactReason === OPTION_QUOTE && E_HEALTH_STATES.indexOf(usState) !==- 1 &&
              (!isWorkingWithBroker || (isWorkingWithBroker && !isContactBrokerTrue))) {
      // contact eHealth
      $rta.val('no');
      $eHealth.val('yes');
      $simplyInsured.val('no');
    }*/
    else if (numEmployees >= 2 && numEmployees <= 50 && contactReason === OPTION_QUOTE && SIMPLY_INSURED_STATES.indexOf(usState) !== -1 && (!isWorkingWithBroker || isWorkingWithBroker && !isContactBrokerTrue)) {
        // contact simplyInsured
        $rta.val('no');
        $eHealth.val('no');
        $simplyInsured.val('yes');
      } else {
        // contact no one
        $rta.val('no');
        $eHealth.val('no');
        $simplyInsured.val('no');
      }
  });
}
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(0)))

/***/ }),
/* 3 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 4 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function($) {

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

/*******************************************************************************
 * dynamicContentLoader.js
 *
 * This file contains all logic for showing/hiding dynamic form elements
 ******************************************************************************/
var _require = __webpack_require__(1),
    OPTION_QUOTE = _require.OPTION_QUOTE,
    OPTION_WELLNESS = _require.OPTION_WELLNESS,
    OPTION_DISCUSS_PRODUCTS = _require.OPTION_DISCUSS_PRODUCTS,
    OPTION_CUSTOMER_SUPPORT = _require.OPTION_CUSTOMER_SUPPORT;
/**
 * adds event handlers to DOM elements
 * 1. dynamic input field generation
 * 2. DOM input value updates
 */


function setupDynamicInputHandlers() {
	/*
  * dynamic input field generation handlers
  */
	displayOnVisitorReason();
	displayOnRadioBtn('cboBroker-rad-yes', 'cboBroker-rad-no', 'show-on-broker');
	displayOnRadioBtn('cboRegard-rad-yes', 'cboRegard-rad-no', 'show-on-broker-contact');
	displayOnRadioBtn('cboOfferBenefits-rad-yes', 'cboOfferBenefits-rad-no', 'show-on-employee-benefit');
	displayEligibility();
}

/**
 * Depending on what user selects from the drop down menu,
 * form will have dynamic fields.
 */
function displayOnVisitorReason() {
	var _optionToBehavior;

	var $txtWhy = $('#txtwhy');

	/*
  * @getQuoteFields (Jquery object) : collection of DOM elements that should be shown
  * if user selected 'Get a quote'
  */
	var $getQuoteFields = $('.show-on-get-quote');

	/*
  * @wellnessFields (Jquery object) : collection of DOM elements that should be shown
  * if user selected 'Learn more about wellness'
  */
	var $wellnessFields = $('.show-on-learn-more-about-wellness');

	/*
  * @productsFields (Jquery Object) : collection of DOM elements that should be
  * shown if user selected 'Discuss our products/services'
  */
	var $productsFields = $('.show-on-discuss-our-products');

	/*
  * @quote$FieldsArr (Jquery object) : collection of DOM elements that should be shown
  * if user selected 'Get customer support'
  */
	var $customerSupportFieldsArr = $('.show-on-get-customer-support');

	/*
  * provides mapping from user selection to deciding what DOM elements to show and hide
  */
	var optionToBehavior = (_optionToBehavior = {}, _defineProperty(_optionToBehavior, OPTION_QUOTE, function () {
		$getQuoteFields.removeClass('hidden');
	}), _defineProperty(_optionToBehavior, OPTION_WELLNESS, function () {
		$wellnessFields.removeClass('hidden');
	}), _defineProperty(_optionToBehavior, OPTION_DISCUSS_PRODUCTS, function () {
		$productsFields.removeClass('hidden');
	}), _defineProperty(_optionToBehavior, OPTION_CUSTOMER_SUPPORT, function () {
		$customerSupportFieldsArr.removeClass('hidden');
	}), _optionToBehavior);
	// display dynamic input fields based on user selection
	$txtWhy.on('change', function (event) {
		// removes all dynamic fields at the beginning
		$wellnessFields.addClass('hidden');
		$productsFields.addClass('hidden');
		$customerSupportFieldsArr.addClass('hidden');
		$getQuoteFields.addClass('hidden');
		var optionPicked = $txtWhy.val();

		// makes sure option is one of the expected values
		if (optionToBehavior.hasOwnProperty(optionPicked)) {
			optionToBehavior[optionPicked]();
		} else {
			// option is not one of the expected values
			/**
    * Usage: thrown when selected option value for the input 'What would you like to do?'
    *								does not match candidate values.
    * What to do if this exception is thrown?
    * Solution: Double check that the <value> attribute in the HTML matches the values
    * in the javascript file (here).
    * Example:
    * HTML : <option value="Get a Quote">Get a quote</option>
    * JS : const OPTION_QUOTE = 'Get a quote';
    * In the html, value of the 'value' attribute does not match the expected value in js.
    * NOTE: Expected values are found in constants.js.
    */
			throw new Error('Error: ' + optionPicked + ' doesn\'t match any of the expected values');
		}
	});
}

/**
 * @param showId (string) : id of the radio button that will display extra fields if selected
 * @param hideId (string) : id of the radio button that will hide extra fields if selected
 * @param fieldClass (string) : every element with this class name will be either shown or hidden depending
 * 								on which radio button is selected
 */
function displayOnRadioBtn(showId, hideId, fieldClass) {
	// Yes button
	var $radioShow = $('#' + showId);
	// No button
	var $radioHide = $('#' + hideId);
	// elements to show/hide based on which button was pressed
	var $fieldsArr = $('.' + fieldClass);

	$radioShow.on('change', function (event) {
		if ($radioShow.is(':checked')) {
			$fieldsArr.removeClass('hidden');
		}
	});

	$radioHide.on('change', function (event) {
		if ($radioHide.is(':checked')) {
			$fieldsArr.addClass('hidden');
		}
	});
}

/**
 * dynamically shows extra questions field depending on user choices for
 * 1. number of employees field
 * 2. State field
 */
function displayEligibility() {
	// constants that refer to the DOM element id of the fields we want
	var STATE_ID = '#txtState';
	var EMPLOYEES_ID = '#noOfEmployees1';
	// jquery objects of the DOM input fields
	var $numEmployees = $(EMPLOYEES_ID);
	var $state = $(STATE_ID);
	// all the fields that will be shown/hidden
	var $fieldsToShow = $('.show-on-eligibility-question');
	/* extra fields show if user selected number of employees matches any entry
  * in allowedNumEmployees
  */
	var allowedNumEmployees = ['2', '3', '4', '5'];
	/*
  * show eligibility question if and only if
  * 1. user selectes 2 for employees fields
  * 2. user selects a state that is not Texas
  * otherwise, eligibility question is not shown
  */
	$(STATE_ID + ', ' + EMPLOYEES_ID).on('blur', function () {
		// show extra fields
		if (allowedNumEmployees.indexOf($numEmployees.val()) > -1 && $state.val() && $state.val() !== 'TX') {
			$fieldsToShow.removeClass('hidden');
		} else {
			// hide extra fields
			$fieldsToShow.addClass('hidden');
		}
	});
}

module.exports = {
	setupDynamicInputHandlers: setupDynamicInputHandlers
};
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(0)))

/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function($) {

/*******************************************************************************
 * formatter.js
 *
 * this file contains all logic related to formatting and adding constraints
 * to form element
 ******************************************************************************/
var _require = __webpack_require__(1),
    ALPHABETIC_DASH_REGEX = _require.ALPHABETIC_DASH_REGEX,
    ALPHABETIC_DASH_SPACE_REGEX = _require.ALPHABETIC_DASH_SPACE_REGEX,
    DATE_REGEX = _require.DATE_REGEX,
    EMAIL_NOT_ALLOWED_REGEX = _require.EMAIL_NOT_ALLOWED_REGEX;
/**
 * call this function
 */


function setupInputFormatting() {
  applyInputRestrictions();
  formatInputs();
}

/**
 * helper function
 *
 * forces input elements to accept/display only valid characters.
 * For example, 'First name' will only accept alphabetic characters and dash,
 * and will not accept numbers.
 */
function applyInputRestrictions() {
  /*
   * All input elements whose valid input is alphabetic characters and dash,
   * will only accept valid characters. Other characters like *&&() etc...
   * will not be accepted and won't be visible
   */
  $('input.alphabetic-dash').on('keydown', function (event) {
    var charPressed = event.key;
    if (!ALPHABETIC_DASH_REGEX.test(charPressed)) {
      event.preventDefault();
    }
  });

  /*
   * All input elements whose valid input is alphabetic characters and dash,
   * will only accept valid characters. Other characters like *&&() etc...
   * will not be accepted and won't be visible
   */
  $('input.name-alphanumeric').on('keydown', function (event) {
    var charPressed = event.key;
    if (!ALPHABETIC_DASH_SPACE_REGEX.test(charPressed)) {
      event.preventDefault();
    }
  });
  /*
   * zip code will only accept numbers as input
   */
  new Cleave('#zipPostal', {
    numeral: true,
    stripLeadingZeroes: false,
    delimiter: ''
  });

  /*
   * if character doesn't match the regular expression DATE_REGEX, then
   * the character is not accepted by the input
   */
  $('#renewalDates').on('keydown', function (event) {
    var charPressed = event.key;
    if (!DATE_REGEX.test(charPressed)) {
      event.preventDefault();
    }
  });

  /*
   * if character matches the regular expression EMAIL_NOT_ALLOWED_REGEX, then
   * the character is not accepted by the input
   */
  $('#txtEmail').on('keydown', function (event) {
    var charPressed = event.key;
    if (EMAIL_NOT_ALLOWED_REGEX.test(charPressed)) {
      event.preventDefault();
    }
  });
}

/**
 * helper function
 *
 * adds formatting to input values
 * NOTE: input formatting uses Cleave.js, which is a small library for formatting
 * <input> elements.
 * Documentation can be found here: https://nosir.github.io/cleave.js/
 */
function formatInputs() {
  /*
   * formats user phone number
   * 2345678900 becomes 234-567-8900
   */
  new Cleave('.input-phone', {
    phone: true,
    phoneRegionCode: 'US',
    blocks: [3, 3, 4],
    delimiter: '-',
    stripLeadingZeroes: true
  });

  /*
  * formats agent phone number
  * 2345678900 becomes 234-567-8900
  */
  new Cleave('.input-agent-phone', {
    phone: true,
    phoneRegionCode: 'US',
    blocks: [3, 3, 4],
    delimiter: '-',
    stripLeadingZeroes: true
  });

  /*
   * formats number of employees
   * 1000 -> 1,000
   * 1000000 -> 1,000,000
   */
  new Cleave('.input-number-employees', {
    numeral: true
  });
}

module.exports = {
  setupInputFormatting: setupInputFormatting
};
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(0)))

/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function($) {

/*******************************************************************************
 * validator.js
 *
 * This file contains all logic related to validating the form.
 * Validation uses Jquery validation plug-in.
 * The documentation can be found here: https://jqueryvalidation.org/
 ******************************************************************************/
var _require = __webpack_require__(1),
    FORM_ID = _require.FORM_ID,
    ALPHABETIC_DASH_REGEX = _require.ALPHABETIC_DASH_REGEX,
    ALPHABETIC_DASH_SPACE_REGEX = _require.ALPHABETIC_DASH_SPACE_REGEX,
    EMAIL_REGEX = _require.EMAIL_REGEX,
    EMAIL_NOT_ALLOWED_REGEX = _require.EMAIL_NOT_ALLOWED_REGEX,
    DATE_REGEX = _require.DATE_REGEX;

function setupFormValidation() {
	// adds custom input validations to the jquery validation plug-in
	initValidationMethods();
	// validate the form
	initFormValidation(FORM_ID);
}

/**
 * helper method
 *
 * This method adds any custom validation methods to the jquery validation plug-in
 * All cusom validation methods can be found towards the bottom of this file
 */
function initValidationMethods() {
	$.validator.addMethod('isAlphabeticDash', isAlphabeticDash, 'Only alphabetic and dashes are accepted. Please try again');
	$.validator.addMethod('isEmailValid', isEmailValid, 'Please enter a valid email address. The following characters are not accepted: ,()[]\\:";<>');
	$.validator.addMethod('isNonZero', isNonZero, 'Please enter a non-zero value');
	$.validator.addMethod('isValidPhone', isValidPhone, 'Please enter a valid U.S. phone number');
	$.validator.addMethod('isAlphabeticDashSpace', isAlphabeticDashSpace, 'Only alphabetic characters, dashes, and spaces are accepted. Please try again');
	$.validator.addMethod('isValidDate', isValidDate, 'Only numeric values and - /, are accepted. Please try again');
	$.validator.addMethod('isValidNumberOfEmployees', isValidNumberOfEmployees, 'Number of employees must be greater than or equal to 2');
}

/**
 * validates the specified form
 * @param (string) : id of the form to validate
 */
function initFormValidation(formId) {
	var $form = $('#' + formId);
	$form.validate({
		validClass: 'valid',
		errorClass: 'invalid',
		/*
   * validation rules for all of the form elements
   */
		rules: {
			txtFirstName: {
				required: true,
				minlength: 2,
				isAlphabeticDash: true
			},
			txtLastName: {
				required: true,
				minlength: 2,
				isAlphabeticDashSpace: true
			},
			txtEmail: {
				required: true,
				isEmailValid: true
			},
			txtPhone: {
				required: true,
				isValidPhone: true
			},
			txtp: {
				required: true
			},
			txtCompany: {
				required: true,
				minlength: 2,
				maxlength: 100
			},
			noOfEmployees1: {
				required: true,
				isNonZero: true,
				maxlength: 13,
				isValidNumberOfEmployees: true
			},
			zipPostal: {
				required: true,
				digits: true
			},
			txtState: {
				required: true
			},
			eligibility: {
				required: true
			},
			txtwhy: {
				required: true
			},
			cboBroker: {
				required: true
			},
			cboOfferBenefits: {
				required: true
			},
			cboRegard: {
				required: true
			},
			agentName: {
				required: true,
				isAlphabeticDashSpace: true
			},
			cboHelp: {
				required: true
			},
			txtGroupMemberId: {
				required: true
			},
			renewalDates: {
				required: true,
				isValidDate: true
			}
		},
		// Error message specification
		messages: {
			txtFirstName: {
				required: 'First name is required',
				isAlphabeticDash: 'Only alphabetic and dashes are accepted. Please try again',
				minlength: 'Please enter at least two characters'
			},
			txtLastName: {
				required: 'Last name is required',
				isAlphabeticDashSpace: 'Only alphabetic characters, dashes, and spaces are accepted. Please try again',
				minlength: 'Please enter at least two characters'
			},
			txtEmail: {
				required: 'Email address is required',
				isEmailValid: 'Please enter a valid email address. The following characters are not accepted: ,()[]\\:";<>'
			},
			txtPhone: {
				required: 'Phone number is required',
				isValidPhone: 'Please enter a valid U.S. phone number'
			},
			txtp: {
				required: 'Please select a preferred contact method'
			},
			txtCompany: {
				required: 'Company name is required',
				minlength: 'Please enter at least two characters',
				maxlength: 'Company name must be 100 characters or less. Please try again'
			},
			noOfEmployees1: {
				required: 'Number of employees is required',
				maxlength: 'Number of employees must be 10 characters or less. Please try again'
			},
			zipPostal: {
				required: 'ZIP code is required',
				minlength: 'Please enter a valid ZIP code'
			},
			txtState: {
				required: 'State selection is required'
			},
			eligibility: {
				required: 'Please select an option'
			},
			txtwhy: {
				required: 'Please select an option'
			},
			cboBroker: {
				required: 'Please select an option'
			},
			cboOfferBenefits: {
				required: 'Please select an option'
			},
			cboRegard: {
				required: 'Please select an option'
			},
			agentName: {
				required: 'Please enter agent\'s full name',
				isAlphabeticDashSpace: 'Only alphabetic characters, dashes, and spaces are accepted. Please try again'
			},
			cboHelp: {
				required: 'Please select an option'
			},
			txtGroupMemberId: {
				required: 'Please provide a Group ID'
			},
			renewalDates: {
				required: 'Renewal Date is required'
			}
		},
		/* specify where error messages should be displayed*/
		errorPlacement: function errorPlacement(error, element) {
			var $inputBox = $(element);
			while (!$inputBox.hasClass('input-box')) {
				$inputBox = $inputBox.parent();
			}
			// place the error message after parent container with class 'input-box'
			$inputBox.after(error);
		},
		/*trigerred when invalid form is submitted*/
		invalidHandler: function invalidHandler(event, validator) {
			// show error message below the submit button
			$('.submit-invalid-msg').removeClass('hidden');
		},
		/*Form submission logic*/
		submitHandler: function submitHandler(form, e) {
			// show the loading spinner
			$('.submit-loading-bar').removeClass('hidden');
			// disable submit button
			$('.submit-btn').prop('disabled', true);
			var numEmployees = $('#noOfEmployees1').val().toString();
			$('#noOfEmployees1').val(parseInt(numEmployees.replace(/,/g, '')));
			$.ajax({
				type: 'POST',
				url: 'https://s1579.t.eloqua.com/e/f2',
				data: $form.serialize(),
				success: function success(data, textStatus) {
					/*
      * data is a script tag in a text format.
      * The script contains logic for redirecting upon form submission.
      * We add this script inside document body to execute it
      */
					$(data).appendTo(document.body);
				},
				/* for unexpected form submission failure
     * do cleanup
     */
				error: function error(textStatus, _error) {
					$('.submit-loading-bar').addClass('hidden');
					$('.submit-btn').prop('disabled', false);
					alert('Something went wrong. Please try again.');
				}
			});
		}
	});
}

/**
 * custom validation method for the jquery validation plug-in
 * @param value (string) : the value of the input field we're checking
 * @return (boolean) : true if value is alphabetic and/or has -
 *										 false otherwise
 */
function isAlphabeticDash(value) {
	return value.search(ALPHABETIC_DASH_REGEX) !== -1;
}

/**
 * custom validation method for the jquery validation plug-in
 * @param value (string) : the value of the input field we're checking
 * @return (boolean) : true if value is alphabetic and/or has dash('-'), and/or space(' ')
 *										 false otherwise
 */
function isAlphabeticDashSpace(value) {
	return value.search(ALPHABETIC_DASH_SPACE_REGEX) !== -1;
}

/**
 * custom validation method for the jquery validation plug-in
 * @param value (string) : the value of the input field we're checking
 * @return (boolean) : true if value is 0, 1, or more characters from
 *										 the regular expression <EMAIL_REGEX>
 *										 false otherwise
 */
function isEmailValid(value) {
	return value.search(EMAIL_REGEX) !== -1;
}

/**
 * custom validation method for the jquery validation plug-in
 * @param value (string) : the value of the input we're checking
 * @return (boolean) : true if value is not zero
 *								     false otherwise
 */
function isNonZero(value) {
	return parseInt(value) !== 0;
}

/**
 * @return (boolean) : true if user entered full phone number, and phone number doesn't start with 1
 *                     excluding country code,
 *                     false otherwise
 * NOTE: this method assumes that user can only enter numbers.
 */
function isValidPhone(value) {
	// if user entered country code, full phone number consists of 14 characters
	// (11 numbers + 3 '-')
	if (value[0] === '1') {
		// valid U.S. phone numbers do not start with 1.
		if (value.length >= 2 && value[1] === '1') return false;
		return value.length === 14;
	} else {
		// otherwise, full phone number consists of 12 characters (10 numbers + 2 '-')
		return value.length === 12;
	}
}

/**
 * @return (boolean) : true if users characters allowed in the date field
 *                     false otherwise
 */
function isValidDate(value) {
	return value.search(DATE_REGEX) !== -1;
}

/**
 * @return (boolean) : true if input is a number >= 2
 *										 false otherwise
 */
function isValidNumberOfEmployees(value) {
	var numEmployees = parseInt(value);
	return numEmployees >= 2;
}

module.exports = {
	setupFormValidation: setupFormValidation
};
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(0)))

/***/ })
],[2]);
});