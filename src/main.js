/***************************************************************************************************************
 * main.js
 * Date: 10/06/2017
 * This file is the project entry point
 ***************************************************************************************************************/
import './resources/css/contact.less';
const {
  //E_HEALTH_STATES,
  RTA_STATES,
  SIMPLY_INSURED_STATES,
  OPTION_QUOTE,
  OPTION_CUSTOMER_SUPPORT
} = require('./constants.js');
const dynamicContentLoader = require('./dynamicContentLoader.js');
const formatter = require('./formatter.js');
const validator = require('./validator.js');

/**
 * Entry point
 */
$(document).ready(function() {
  // handles the logic for showing/hiding dynamic elements
  dynamicContentLoader.setupDynamicInputHandlers();
  // decide where to send information: ehealth or rta
  initRouting();
	// applies constraints/formatting to input elements
  formatter.setupInputFormatting();
  // setup the logic for form validation
  validator.setupFormValidation();
});

/**
 * Depending on user entry, eloqua will send notification email to either
 * 1. Rta
 * 2. eHealth
 * 3. simplyInsured
 */
 function initRouting() {
   const $rta = $('#rta');
   const $eHealth = $('#eHealth');
   const $simplyInsured = $('#SimplyInsured');
   $('.representative-routing').on('change', function() {
     const numEmployees = $('#noOfEmployees1').val();
     const usState = $('#txtState').val();
     const contactReason = $('#txtwhy option:selected').text();
     const isWorkingWithBroker = $('#cboBroker').val() === 'Yes';
     const isContactBrokerTrue = $('#cboRegard').val() === 'Yes';
     if (numEmployees <= 50 && contactReason !== OPTION_CUSTOMER_SUPPORT &&
        RTA_STATES.indexOf(usState) !==- 1 &&
        (!isWorkingWithBroker || (isWorkingWithBroker && !isContactBrokerTrue))) {
       // contact RTA
       $rta.val('yes');
       $eHealth.val('no');
       $simplyInsured.val('no');
     } 
     /*else if (numEmployees<=50 &&
               contactReason === OPTION_QUOTE && E_HEALTH_STATES.indexOf(usState) !==- 1 &&
               (!isWorkingWithBroker || (isWorkingWithBroker && !isContactBrokerTrue))) {
       // contact eHealth
       $rta.val('no');
       $eHealth.val('yes');
       $simplyInsured.val('no');
     }*/ 
     else if (numEmployees>=2 && numEmployees<=50 &&
               contactReason === OPTION_QUOTE && SIMPLY_INSURED_STATES.indexOf(usState) !==- 1 &&
               (!isWorkingWithBroker || (isWorkingWithBroker && !isContactBrokerTrue))) {
       // contact simplyInsured
       $rta.val('no');
       $eHealth.val('no');
       $simplyInsured.val('yes');

     } else {
       // contact no one
       $rta.val('no');
       $eHealth.val('no');
       $simplyInsured.val('no');
     }
   });
 }