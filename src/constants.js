/*******************************************************************************
 * constants.js
 *
 * This file contains all the constants
 ******************************************************************************/
/**
 * select input options for 'What would you like to do?'
 * if you change the values in the HTML, be sure to update here as well.
 * NOTE: if the value here doesn't match the corresponding <option> element value of the <value> attribute,
 *			  exception 'invalidSelectOption' is thrown.
 */
module.exports = {
  FORM_ID : 'elqForm',
  OPTION_QUOTE : 'Get a quote',
  OPTION_WELLNESS : 'Learn more about wellness',
  OPTION_DISCUSS_PRODUCTS : 'Discuss our products/services',
  OPTION_CUSTOMER_SUPPORT : 'Get customer support',
  EMAIL_REGEX : /[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,9}$/,
  EMAIL_NOT_ALLOWED_REGEX : /^[,()\[\]\\:";<>]+$/,
  /**
   * states subject to RTA routing
   */
  RTA_STATES : [
    'AZ',
    'CO',
    'FL',
    'GA',
    'IL',
    'IN',
    'KS',
    'KY',
    'LA',
    'MI',
    'MO',
    'MS',
    'NV',
    'OH',
    'TN',
    'TX',
    'UT',
    'WI'
  ],
  /**
  * states subject to eHealth routing
  */
  /*E_HEALTH_STATES : [
    'CA',
    'NC',
    'NJ',
    'NY'
  ],*/
  SIMPLY_INSURED_STATES : [
    'VA',
    'WA',
    'AL',
    'PA',
    'SC',
    'CA',
    'NC',
    'NJ',
    'NY'
  ],
  /**
   * @ALPHABETIC_DASH_REGEX : used for first name / last name input validation
   * 												matches any text that has only alphabetic
   *												characters and dash(-).
   */
   ALPHABETIC_DASH_REGEX : /^[a-zA-Z-]+$/,

  /**
   * @ALPHABETIC_DASH_SPACE_REGEX: used for last name and agent name
   *                               matches any text that has only alphabetic,
   *                               and/or - and space.
   */
   ALPHABETIC_DASH_SPACE_REGEX : /^[a-zA-Z-\s]+$/,

  /*
   * @DATE_REGEX: used for renewal date field
   *                      matches any text that has only alphanumeric and/or
   *                      '-', '/', and ',' characters.
   */
   DATE_REGEX : /^[a-zA-Z0-9,-/]+$/,

  /**
   * @EMAIL_EXTRA_CHARS_REGEX : used for email additional character validation
   *												 matches text that is constructed from any of the
   *												 characters within the brackers (i.e. [...])
   */
   EMAIL_EXTRA_CHARS_REGEX : /^[0-9A-Za-z.,~!@#$%^&*()]+$/
};
