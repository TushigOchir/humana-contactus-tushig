/**********************************************
 * This file loads 3rd party vendor libraries.
 * This file is not used directly. Instead a
 * minified version of it is used, which has been created in 'dist' folder.
 **********************************************/
require('jquery');
require('./xDomainRequest.js');
require('jquery-validation');
require('cleave.js');
require('cleave.js/dist/addons/cleave-phone.us');
