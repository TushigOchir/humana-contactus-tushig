/*******************************************************************************
 * validator.js
 *
 * This file contains all logic related to validating the form.
 * Validation uses Jquery validation plug-in.
 * The documentation can be found here: https://jqueryvalidation.org/
 ******************************************************************************/
const {
	FORM_ID,
	ALPHABETIC_DASH_REGEX,
	ALPHABETIC_DASH_SPACE_REGEX,
	EMAIL_REGEX,
	EMAIL_NOT_ALLOWED_REGEX,
	DATE_REGEX
} = require('./constants.js');

function setupFormValidation() {
  // adds custom input validations to the jquery validation plug-in
	initValidationMethods();
	// validate the form
	initFormValidation(FORM_ID);
}

/**
 * helper method
 *
 * This method adds any custom validation methods to the jquery validation plug-in
 * All cusom validation methods can be found towards the bottom of this file
 */
function initValidationMethods() {
	$.validator.addMethod('isAlphabeticDash', isAlphabeticDash, 'Only alphabetic and dashes are accepted. Please try again');
	$.validator.addMethod('isEmailValid', isEmailValid, 'Please enter a valid email address. The following characters are not accepted: ,()[]\\:";<>');
	$.validator.addMethod('isNonZero', isNonZero, 'Please enter a non-zero value');
  $.validator.addMethod('isValidPhone', isValidPhone, 'Please enter a valid U.S. phone number');
  $.validator.addMethod('isAlphabeticDashSpace', isAlphabeticDashSpace, 'Only alphabetic characters, dashes, and spaces are accepted. Please try again');
  $.validator.addMethod('isValidDate', isValidDate, 'Only numeric values and - /, are accepted. Please try again');
	$.validator.addMethod('isValidNumberOfEmployees', isValidNumberOfEmployees, 'Number of employees must be greater than or equal to 2');
}

/**
 * validates the specified form
 * @param (string) : id of the form to validate
 */
function initFormValidation(formId) {
	const $form = $(`#${formId}`);
	$form.validate({
    validClass: 'valid',
    errorClass: 'invalid',
    /*
     * validation rules for all of the form elements
     */
		rules: {
			txtFirstName: {
				required: true,
				minlength: 2,
				isAlphabeticDash: true
			},
			txtLastName: {
				required: true,
				minlength: 2,
				isAlphabeticDashSpace: true
			},
			txtEmail: {
				required: true,
				isEmailValid: true,
			},
			txtPhone: {
				required: true,
        isValidPhone: true
			},
			txtp: {
				required: true
			},
			txtCompany: {
				required: true,
				minlength: 2,
				maxlength: 100
			},
			noOfEmployees1: {
				required: true,
				isNonZero: true,
				maxlength: 13,
				isValidNumberOfEmployees: true
			},
			zipPostal: {
				required: true,
				digits: true
			},
			txtState: {
				required: true
			},
			eligibility: {
				required: true
			},
			txtwhy: {
				required: true
			},
			cboBroker: {
				required: true
			},
			cboOfferBenefits: {
				required: true
			},
			cboRegard: {
				required: true
			},
			agentName: {
				required: true,
				isAlphabeticDashSpace: true
			},
			cboHelp: {
				required: true
			},
			txtGroupMemberId: {
				required: true
			},
      renewalDates: {
        required: true,
        isValidDate: true
      }
		},
		// Error message specification
		messages: {
			txtFirstName: {
				required: 'First name is required',
				isAlphabeticDash: 'Only alphabetic and dashes are accepted. Please try again',
				minlength: 'Please enter at least two characters'
			},
			txtLastName: {
				required: 'Last name is required',
				isAlphabeticDashSpace: 'Only alphabetic characters, dashes, and spaces are accepted. Please try again',
				minlength: 'Please enter at least two characters'
			},
			txtEmail: {
				required: 'Email address is required',
				isEmailValid: 'Please enter a valid email address. The following characters are not accepted: ,()[]\\:";<>'
			},
			txtPhone: {
				required: 'Phone number is required',
        isValidPhone: 'Please enter a valid U.S. phone number'
			},
			txtp: {
				required: 'Please select a preferred contact method'
			},
			txtCompany: {
				required: 'Company name is required',
        minlength: 'Please enter at least two characters',
				maxlength: 'Company name must be 100 characters or less. Please try again'
			},
			noOfEmployees1: {
				required: 'Number of employees is required',
				maxlength: 'Number of employees must be 10 characters or less. Please try again'
			},
			zipPostal: {
				required: 'ZIP code is required',
        minlength: 'Please enter a valid ZIP code'
			},
			txtState: {
				required: 'State selection is required'
			},
			eligibility: {
				required: 'Please select an option'
			},
			txtwhy: {
				required: 'Please select an option'
			},
			cboBroker: {
				required: 'Please select an option'
			},
			cboOfferBenefits: {
				required: 'Please select an option'
			},
			cboRegard: {
				required: 'Please select an option'
			},
			agentName: {
				required: 'Please enter agent\'s full name',
        isAlphabeticDashSpace: 'Only alphabetic characters, dashes, and spaces are accepted. Please try again'
			},
			cboHelp: {
				required: 'Please select an option'
			},
			txtGroupMemberId: {
				required: 'Please provide a Group ID'
			},
      renewalDates: {
        required: 'Renewal Date is required'
      }
		},
    /* specify where error messages should be displayed*/
    errorPlacement: function(error, element) {
      let $inputBox = $(element);
      while (!$inputBox.hasClass('input-box')) {
        $inputBox = $inputBox.parent();
      }
      // place the error message after parent container with class 'input-box'
      $inputBox.after(error);
    },
    /*trigerred when invalid form is submitted*/
    invalidHandler: function(event, validator) {
      // show error message below the submit button
      $('.submit-invalid-msg').removeClass('hidden');
    },
		/*Form submission logic*/
    submitHandler: function(form, e) {
			// show the loading spinner
			$('.submit-loading-bar').removeClass('hidden');
      // disable submit button
			$('.submit-btn').prop('disabled', true);
			var numEmployees = $('#noOfEmployees1').val().toString();
			$('#noOfEmployees1').val(parseInt(numEmployees.replace(/,/g, '')))
			$.ajax({
				type: 'POST',
				url: 'https://s1579.t.eloqua.com/e/f2',
				data: $form.serialize(),
				success: function(data, textStatus) {
					/*
					 * data is a script tag in a text format.
					 * The script contains logic for redirecting upon form submission.
					 * We add this script inside document body to execute it
					 */
					$(data).appendTo(document.body);
				},
				/* for unexpected form submission failure
				 * do cleanup
				 */
				error: function(textStatus, error) {
					$('.submit-loading-bar').addClass('hidden');
					$('.submit-btn').prop('disabled', false);
					alert('Something went wrong. Please try again.');
				}
			});
    }
	});
}

/**
 * custom validation method for the jquery validation plug-in
 * @param value (string) : the value of the input field we're checking
 * @return (boolean) : true if value is alphabetic and/or has -
 *										 false otherwise
 */
function isAlphabeticDash(value) {
	return value.search(ALPHABETIC_DASH_REGEX) !== -1;
}

/**
 * custom validation method for the jquery validation plug-in
 * @param value (string) : the value of the input field we're checking
 * @return (boolean) : true if value is alphabetic and/or has dash('-'), and/or space(' ')
 *										 false otherwise
 */
function isAlphabeticDashSpace(value) {
  return value.search(ALPHABETIC_DASH_SPACE_REGEX) !== -1;
}

/**
 * custom validation method for the jquery validation plug-in
 * @param value (string) : the value of the input field we're checking
 * @return (boolean) : true if value is 0, 1, or more characters from
 *										 the regular expression <EMAIL_REGEX>
 *										 false otherwise
 */
function isEmailValid(value) {
  return value.search(EMAIL_REGEX) !== -1;
}

/**
 * custom validation method for the jquery validation plug-in
 * @param value (string) : the value of the input we're checking
 * @return (boolean) : true if value is not zero
 *								     false otherwise
 */
function isNonZero(value) {
	return parseInt(value) !== 0;
}

/**
 * @return (boolean) : true if user entered full phone number, and phone number doesn't start with 1
 *                     excluding country code,
 *                     false otherwise
 * NOTE: this method assumes that user can only enter numbers.
 */
function isValidPhone(value) {
  // if user entered country code, full phone number consists of 14 characters
  // (11 numbers + 3 '-')
  if (value[0] === '1') {
    // valid U.S. phone numbers do not start with 1.
    if (value.length >= 2 && value[1] === '1') return false;
    return value.length === 14;
  } else { // otherwise, full phone number consists of 12 characters (10 numbers + 2 '-')
    return value.length === 12;
  }
}

/**
 * @return (boolean) : true if users characters allowed in the date field
 *                     false otherwise
 */
function isValidDate(value) {
  return value.search(DATE_REGEX) !== -1;
}

/**
 * @return (boolean) : true if input is a number >= 2
 *										 false otherwise
 */
function isValidNumberOfEmployees(value) {
	const numEmployees = parseInt(value);
	return numEmployees >= 2;
}

module.exports = {
	setupFormValidation
};
