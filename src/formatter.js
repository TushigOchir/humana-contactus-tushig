/*******************************************************************************
 * formatter.js
 *
 * this file contains all logic related to formatting and adding constraints
 * to form element
 ******************************************************************************/
const {
  ALPHABETIC_DASH_REGEX,
  ALPHABETIC_DASH_SPACE_REGEX,
  DATE_REGEX,
  EMAIL_NOT_ALLOWED_REGEX
} = require('./constants.js');
/**
 * call this function
 */
function setupInputFormatting() {
  applyInputRestrictions();
  formatInputs();
}

/**
 * helper function
 *
 * forces input elements to accept/display only valid characters.
 * For example, 'First name' will only accept alphabetic characters and dash,
 * and will not accept numbers.
 */
function applyInputRestrictions() {
  /*
   * All input elements whose valid input is alphabetic characters and dash,
   * will only accept valid characters. Other characters like *&&() etc...
   * will not be accepted and won't be visible
   */
  $('input.alphabetic-dash').on('keydown', function(event) {
    const charPressed = event.key;
    if (!ALPHABETIC_DASH_REGEX.test(charPressed)) {
      event.preventDefault();
    }
  });

  /*
   * All input elements whose valid input is alphabetic characters and dash,
   * will only accept valid characters. Other characters like *&&() etc...
   * will not be accepted and won't be visible
   */
  $('input.name-alphanumeric').on('keydown', function(event) {
    const charPressed = event.key;
    if (!ALPHABETIC_DASH_SPACE_REGEX.test(charPressed)) {
      event.preventDefault();
    }
  });
  /*
   * zip code will only accept numbers as input
   */
  new Cleave('#zipPostal', {
    numeral: true,
    stripLeadingZeroes: false,
    delimiter: ''
  });

  /*
   * if character doesn't match the regular expression DATE_REGEX, then
   * the character is not accepted by the input
   */
  $('#renewalDates').on('keydown', function(event) {
    const charPressed = event.key;
    if (!DATE_REGEX.test(charPressed)) {
      event.preventDefault();
    }
  });

  /*
   * if character matches the regular expression EMAIL_NOT_ALLOWED_REGEX, then
   * the character is not accepted by the input
   */
  $('#txtEmail').on('keydown', function(event) {
    const charPressed = event.key;
    if (EMAIL_NOT_ALLOWED_REGEX.test(charPressed)) {
      event.preventDefault();
    }
  });
}

/**
 * helper function
 *
 * adds formatting to input values
 * NOTE: input formatting uses Cleave.js, which is a small library for formatting
 * <input> elements.
 * Documentation can be found here: https://nosir.github.io/cleave.js/
 */
function formatInputs() {
	/*
	 * formats user phone number
	 * 2345678900 becomes 234-567-8900
	 */
	new Cleave('.input-phone', {
		phone: true,
		phoneRegionCode: 'US',
		blocks: [3, 3, 4],
		delimiter: '-',
		stripLeadingZeroes: true
	});

  /*
	 * formats agent phone number
	 * 2345678900 becomes 234-567-8900
	 */
	new Cleave('.input-agent-phone', {
		phone: true,
		phoneRegionCode: 'US',
		blocks: [3, 3, 4],
		delimiter: '-',
		stripLeadingZeroes: true
	});

	/*
	 * formats number of employees
	 * 1000 -> 1,000
	 * 1000000 -> 1,000,000
	 */
	new Cleave('.input-number-employees', {
		numeral: true
	});
}

module.exports = {
  setupInputFormatting
};
