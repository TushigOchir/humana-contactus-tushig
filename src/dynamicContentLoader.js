/*******************************************************************************
 * dynamicContentLoader.js
 *
 * This file contains all logic for showing/hiding dynamic form elements
 ******************************************************************************/
const {
	OPTION_QUOTE,
	OPTION_WELLNESS,
	OPTION_DISCUSS_PRODUCTS,
	OPTION_CUSTOMER_SUPPORT
} = require('./constants.js');
/**
 * adds event handlers to DOM elements
 * 1. dynamic input field generation
 * 2. DOM input value updates
 */
function setupDynamicInputHandlers() {
	/*
	 * dynamic input field generation handlers
	 */
	displayOnVisitorReason();
	displayOnRadioBtn('cboBroker-rad-yes', 'cboBroker-rad-no', 'show-on-broker');
	displayOnRadioBtn('cboRegard-rad-yes', 'cboRegard-rad-no', 'show-on-broker-contact');
	displayOnRadioBtn('cboOfferBenefits-rad-yes', 'cboOfferBenefits-rad-no', 'show-on-employee-benefit');
	displayEligibility();
}

/**
 * Depending on what user selects from the drop down menu,
 * form will have dynamic fields.
 */
function displayOnVisitorReason() {
	const $txtWhy = $('#txtwhy');

	/*
	 * @getQuoteFields (Jquery object) : collection of DOM elements that should be shown
	 * if user selected 'Get a quote'
	 */
	const $getQuoteFields = $('.show-on-get-quote');

	/*
	 * @wellnessFields (Jquery object) : collection of DOM elements that should be shown
	 * if user selected 'Learn more about wellness'
	 */
	const $wellnessFields = $('.show-on-learn-more-about-wellness');

	/*
	 * @productsFields (Jquery Object) : collection of DOM elements that should be
	 * shown if user selected 'Discuss our products/services'
	 */
	const $productsFields = $('.show-on-discuss-our-products');

	/*
	 * @quote$FieldsArr (Jquery object) : collection of DOM elements that should be shown
	 * if user selected 'Get customer support'
	 */
	const $customerSupportFieldsArr = $('.show-on-get-customer-support');

	/*
	 * provides mapping from user selection to deciding what DOM elements to show and hide
	 */
	const optionToBehavior = {
		[OPTION_QUOTE] : function() {
			$getQuoteFields.removeClass('hidden');
		},
		[OPTION_WELLNESS] : function() {
			$wellnessFields.removeClass('hidden');
		},
		[OPTION_DISCUSS_PRODUCTS] : function() {
			$productsFields.removeClass('hidden');
		},
		[OPTION_CUSTOMER_SUPPORT] : function() {
			$customerSupportFieldsArr.removeClass('hidden');
		}
	};
	// display dynamic input fields based on user selection
	$txtWhy.on('change', (event) => {
		// removes all dynamic fields at the beginning
		$wellnessFields.addClass('hidden');
		$productsFields.addClass('hidden');
		$customerSupportFieldsArr.addClass('hidden');
		$getQuoteFields.addClass('hidden');
		const optionPicked = $txtWhy.val();

		// makes sure option is one of the expected values
		if (optionToBehavior.hasOwnProperty(optionPicked)) {
			optionToBehavior[optionPicked]();
		} else { // option is not one of the expected values
      /**
       * Usage: thrown when selected option value for the input 'What would you like to do?'
       *								does not match candidate values.
       * What to do if this exception is thrown?
       * Solution: Double check that the <value> attribute in the HTML matches the values
       * in the javascript file (here).
       * Example:
       * HTML : <option value="Get a Quote">Get a quote</option>
       * JS : const OPTION_QUOTE = 'Get a quote';
       * In the html, value of the 'value' attribute does not match the expected value in js.
       * NOTE: Expected values are found in constants.js.
       */
      throw new Error(`Error: ${optionPicked} doesn't match any of the expected values`);
		}
	});
}

/**
 * @param showId (string) : id of the radio button that will display extra fields if selected
 * @param hideId (string) : id of the radio button that will hide extra fields if selected
 * @param fieldClass (string) : every element with this class name will be either shown or hidden depending
 * 								on which radio button is selected
 */
function displayOnRadioBtn(showId, hideId, fieldClass) {
  // Yes button
	const $radioShow = $(`#${showId}`);
  // No button
	const $radioHide = $(`#${hideId}`);
  // elements to show/hide based on which button was pressed
	const $fieldsArr = $(`.${fieldClass}`);

	$radioShow.on('change', (event) => {
		if ($radioShow.is(':checked')) {
			$fieldsArr.removeClass('hidden');
		}
	});

	$radioHide.on('change', (event) => {
		if ($radioHide.is(':checked')) {
			$fieldsArr.addClass('hidden');
		}
	});
}

/**
 * dynamically shows extra questions field depending on user choices for
 * 1. number of employees field
 * 2. State field
 */
function displayEligibility() {
	// constants that refer to the DOM element id of the fields we want
	const STATE_ID = '#txtState';
	const EMPLOYEES_ID = '#noOfEmployees1';
	// jquery objects of the DOM input fields
	const $numEmployees = $(EMPLOYEES_ID);
	const $state = $(STATE_ID);
	// all the fields that will be shown/hidden
	const $fieldsToShow = $('.show-on-eligibility-question');
	/* extra fields show if user selected number of employees matches any entry
	 * in allowedNumEmployees
	 */
	const allowedNumEmployees = ['2', '3', '4', '5']
	/*
	 * show eligibility question if and only if
	 * 1. user selectes 2 for employees fields
	 * 2. user selects a state that is not Texas
	 * otherwise, eligibility question is not shown
	 */
	$(`${STATE_ID}, ${EMPLOYEES_ID}`).on('blur', function() {
		// show extra fields
		if (allowedNumEmployees.indexOf($numEmployees.val()) > -1 && ($state.val() && $state.val() !== 'TX')) {
			$fieldsToShow.removeClass('hidden');
		} else { // hide extra fields
			$fieldsToShow.addClass('hidden');
		}
	});
}

module.exports = {
	setupDynamicInputHandlers
};
